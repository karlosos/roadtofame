#include <exception>
using namespace std;

#ifndef __PrototypWoda_h__
#define __PrototypWoda_h__

#include "pu�apka.h"
#include "Pulapka.h"
#include "Prototyp.h"

// class pu�apka;
// class Pulapka;
// class Prototyp;
class PrototypWoda;

class PrototypWoda: public pu�apka, public Pulapka, public Prototyp
{
	protected: double _x;
	protected: double _y;

	public: void wykryjKolizje();

	public: void woda();

	public: void klonuj();
};

#endif
