#include <exception>
using namespace std;

#ifndef __menu_h__
#define __menu_h__

// #include "baza_danych.h"
#include "player.h"

class baza_danych;
// class player;
class menu;

/**
 * miejsce w kt�rym gracz mo�e wybra� opcje, kt�re wykonaj� r�ne akcje
 */
class menu: public player, public player
{
	private: bool _muyzka;
	public: baza_danych* _unnamed_baza_danych_;

	public: void startGry();

	public: bool w��czMuzyk�();

	public: bool wy��czMuzyk�();

	public: bool owt�rzBazeDanych();

	public: void opuscGre();

	public: void ustawRozdzielczosc();
};

#endif
