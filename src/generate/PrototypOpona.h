#include <exception>
using namespace std;

#ifndef __PrototypOpona_h__
#define __PrototypOpona_h__

#include "pu�apka.h"
#include "Pulapka.h"
#include "Prototyp.h"

// class pu�apka;
// class Pulapka;
// class Prototyp;
class PrototypOpona;

class PrototypOpona: public pu�apka, public Pulapka, public Prototyp
{
	protected: double _x;
	protected: double _y;

	public: void wykryjKolizje();
			void klonuj();
			void setX();
			void setY();
};

#endif
