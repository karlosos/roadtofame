#include <exception>
using namespace std;

#ifndef __ubytek_jezdni_h__
#define __ubytek_jezdni_h__

#include "pu�apka.h"
#include "Pulapka.h"
#include "Prototyp.h"

// class pu�apka;
// class Pulapka;
// class Prototyp;
class ubytek_jezdni;

class ubytek_jezdni: public pu�apka, public Pulapka, public Prototyp
{
	protected: double _x;
	protected: double _y;

	public: void wykryjKoliz();

	public: void ubytekjezdni();

	public: void klonuj();

	public: void operation();
};

#endif
