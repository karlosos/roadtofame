#include <exception>
#include <string>
using namespace std;

#ifndef __player_h__
#define __player_h__

// #include "Auto3.h"

class Auto3;
class player;

/**
 * (inaczej gracz), osoba kt�a kieruje samochodem
 */
class player
{
	private: string _name;
	private: int _wiek;
	public: player* _unnamed_player_;
	public: Auto3* _posiada;

	public: string ustawImie();

	public: int ustawWiek();
};

#endif
