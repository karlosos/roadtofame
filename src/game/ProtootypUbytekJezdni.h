#pragma once
#pragma once
#ifndef __PrototypUbytekJezdni_h__
#define __PrototypUbytekJezdni_h__

#include "Pulapka.h"
#include "PrototypPrzeszkody.h"
///#include "Auto.h"

class PrototypUbytekJezdni : public Pulapka
{
public:
	PrototypUbytekJezdni();
	~PrototypUbytekJezdni();

public: void wykryjKolizje();
		void jezdnia();
		PrototypPrzeszkody* klonuj();

};

#endif

