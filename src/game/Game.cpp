#include "Game.h"
Game * Game::_instancja = nullptr;
Game::Game()
{
	przywitanie();
	przygotujAuto();
	przygotujTor();
	przygotujPulapki();
}


Game::~Game()
{
}

Game * Game::getInstancja() {
	if (Game::_instancja == nullptr)
		Game::_instancja = new Game();
	return _instancja;
}

void Game::rozgrywka() {
	while (1) {
		// jezeli przejechal caly tor wyjdz z petli gry
		if (_unnamed_tor->jedzPoTorze()) {
			break;
		}

		// sprawdzaj kolizje dla kazdej przeszkody
		for (auto &przeszkoda : przeszkody) {
			przeszkoda->wykryjKolizje();
		}

	}
}

void Game::przywitanie() {
	// 1. Przywitanie w grze
	std::cout << "Witaj w grze!" << std::endl;
	_unnamed_player = new Player();
	podajImieGracza();
	podajWiekGracza();
}

void Game::podajImieGracza() {
	std::string imie;
	std::cout << "Podaj imie gracza: ";
	std::cin >> imie;
}

void Game::podajWiekGracza() {
	// 3. TODO Napisac funkcje ktora ustawia wiek gracza
	int wiek;
	std::cout << "Podaj wiek gracza: ";
	std::cin >> wiek;
}

void Game::przygotujAuto() {
	// 4. Tworze auto. 
	_unnamed_Auto = new Auto();
	// ustaw relacje auto_gracza <-> player
	_unnamed_player->_posiada = _unnamed_Auto;
	_unnamed_Auto->_posiada = _unnamed_player;

	// 5. Ustawiam kolor auta.
	_unnamed_Auto->zmienKolor();

	// 6. Zatankuj 
	_unnamed_Auto->zatankuj();
}

void Game::przygotujTor() {
	// 7. Stworzenie toru
	_unnamed_tor = new Tor(100);
	// ustaw relacje tor <-> auto_gracza
	_unnamed_tor->_unnamed_Auto_ = _unnamed_Auto;
	_unnamed_Auto->_unnamed_tor_ = _unnamed_tor;
}

void Game::przygotujPulapki() {
	PrototypOpona opona;
	opona._unnamed_Auto = _unnamed_Auto;
	PrototypSmar smar;
	smar._unnamed_Auto = _unnamed_Auto;
	PrototypUbytekJezdni ubytek_jezdni;
	ubytek_jezdni._unnamed_Auto = _unnamed_Auto;
	PrototypWoda woda;
	woda._unnamed_Auto = _unnamed_Auto;
	
	generujPulapki(&opona);
	generujPulapki(&smar);
	generujPulapki(&ubytek_jezdni);
	generujPulapki(&woda);
}

void Game::generujPulapki(PrototypPrzeszkody* prot) {
	// stworz plamy wody
	int liczba_pulapek = std::rand() % 10 + 2;
	for (int i = 0; i < liczba_pulapek; i++) {
		PrototypPrzeszkody* klon = prot->klonuj();
		// losuj pozycje wody
		double new_x = std::rand() % 100;
		klon->setX(new_x);
		double new_y = 0;
		klon->setY(new_y);
		przeszkody.push_back(klon);
	}
}