#include "ProtootypUbytekJezdni.h"
#include "Pulapka.h"
#include "PrototypPrzeszkody.h"
#include "Auto.h"
#include <iostream>
using namespace std;


PrototypUbytekJezdni::PrototypUbytekJezdni()
{
	//cout << "Konstruktor PrototypUbytekJezdni" << endl;
}


PrototypUbytekJezdni::~PrototypUbytekJezdni()
{
	//cout << "Destruktor PrototypUbytekJezdni" << endl;
}

void PrototypUbytekJezdni::wykryjKolizje() {
	if (this->_unnamed_Auto->getX() == this->_x && this->_unnamed_Auto->getY() == this->_y) {
		cout << "Auto wpad�o w kolizje z Ubytkiem w jezdni" << endl;
	}
}

void PrototypUbytekJezdni::jezdnia() {
	PrototypUbytekJezdni jezdnia = PrototypUbytekJezdni();
}

PrototypPrzeszkody* PrototypUbytekJezdni::klonuj() {
	PrototypUbytekJezdni* sklonowany;
	sklonowany = new PrototypUbytekJezdni();
	sklonowany->setX(this->_x);
	sklonowany->setY(this->_y);
	sklonowany->_unnamed_Auto = this->_unnamed_Auto;
	return sklonowany;
}