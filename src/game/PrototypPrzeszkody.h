#pragma once

#ifndef __Prototyp_h__
#define __Prototyp_h__

class PrototypPrzeszkody
{
public:
	virtual PrototypPrzeszkody* klonuj() = 0;
	void setX(double x);
	void setY(double y);
	void virtual wykryjKolizje() = 0;
protected: 
	double _x;
	double _y;

};

#endif
