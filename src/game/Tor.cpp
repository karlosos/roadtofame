#include <iostream>
#include "Tor.h"
#include "Auto.h"

using namespace std;

Tor::Tor(double dlugosc)
{
	this->_dlugosc = dlugosc;
	//cout << "Konstruktor Tor" << endl;
}

Tor::~Tor()
{
	//cout << "Destruktor Tor" << endl;
}

bool Tor::jedzPoTorze() {
	this->_unnamed_Auto_->jedzDoPrzodu();
	cout << "Auto jezdzi po torze. Pozycja x:" << this->_unnamed_Auto_->getX() << " y: " << this->_unnamed_Auto_->getY() << endl;
	// tor ma 100 dlugosci
	if (this->_unnamed_Auto_->getX() >= this->_dlugosc) {
		cout << "Koniec toru!" << endl;
		return true;
	}
	return false;
}
