#pragma once
#include <vector>
using namespace std;


#include "Pulapka.h"
#include "Player.h"
#include "Tor.h"

class Pulapka;
class Player;
class Tor;

/**
* obiekt kt�ry posiada centrum ci�ko�ci i rozmiary i kszta�t
*/
class Auto
{
public:
	Auto();
	~Auto();
private: int _kolor;
private: bool _silnik = false;
private: bool _hamowanie = false;
private: int _uderzenie;
private: float _wspolrzedne;
private: double _x;
private: double _y;
private: int _paliwo = 0;
public: Pulapka * _unnamed_pulapka;
public: Player * _posiada;
public: Tor * _unnamed_tor_;

public: bool zmienKolor();

public: void odpalAuto();

public: bool wykryjuderzenie();

public: void zatankuj();

public: void setX(double x);
public: void setY(double y);
public: double getX();
public: double getY();
public: void jedzDoPrzodu();
public: void setHamuj();
public: bool getHamuj();
public: void skrecWPrawo();
public: void skrecWLewo();
};
