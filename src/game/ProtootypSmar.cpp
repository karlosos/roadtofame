#include "ProtootypSmar.h"
#include "Pulapka.h"
#include "PrototypPrzeszkody.h"
#include <iostream>
using namespace std;


PrototypSmar::PrototypSmar()
{
	//cout << "Konstruktor Smar" << endl;
}


PrototypSmar::~PrototypSmar()
{
	//cout << "Destruktor Smar" << endl;
}

void PrototypSmar::wykryjKolizje() {
	if (this->_unnamed_Auto->getX() == this->_x && this->_unnamed_Auto->getY() == this->_y) {
		cout << "Auto wpad�o w kolizje z Smarem" << endl;
	}
}

void PrototypSmar::smar() {
	PrototypSmar smar = PrototypSmar();
}

PrototypPrzeszkody* PrototypSmar::klonuj() {
	PrototypSmar* sklonowany;
	sklonowany = new PrototypSmar();
	sklonowany->setX(this->_x);
	sklonowany->setY(this->_y);
	sklonowany->_unnamed_Auto = this->_unnamed_Auto;
	return sklonowany;
}


