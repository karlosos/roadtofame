#pragma once
#ifndef __PrototypOpona_h__
#define __PrototypOpona_h__

#include "Pulapka.h"
#include "PrototypPrzeszkody.h"
#include "Auto.h"

class PrototypOpona : public Pulapka
{
public: void wykryjKolizje();
		void opona();
		PrototypPrzeszkody* klonuj();
};

#endif