#include "ProtootypOpona.h"
#include "Pulapka.h"
#include "PrototypPrzeszkody.h"
#include <iostream>
using namespace std;

void PrototypOpona::wykryjKolizje()
{
	if (this->_unnamed_Auto->getX() == this->_x && this->_unnamed_Auto->getY() == this->_y) {
		cout << "Auto wpadlo w kolizje z Opona" << endl;
	}
}

void PrototypOpona::opona() {
	PrototypOpona opna = PrototypOpona();
}

PrototypPrzeszkody* PrototypOpona::klonuj() {
	PrototypOpona* sklonowany;
	sklonowany = new PrototypOpona();
	sklonowany->setX(this->_x);
	sklonowany->setY(this->_y);
	sklonowany->_unnamed_Auto = this->_unnamed_Auto;
	return sklonowany;
}

