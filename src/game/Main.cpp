#include "Auto.h"
#include "BazaDanych.h"
#include "Menu.h"
#include "Player.h"
#include "PrototypPrzeszkody.h"
#include "ProtootypOpona.h"
#include "ProtootypSmar.h"
#include "ProtootypUbytekJezdni.h"
#include "ProtootypWoda.h"
#include "Pulapka.h"
#include "Tor.h"

#include <ctime>
#include <iostream>
#include "Game.h"
using namespace std;

int main() {
	std::srand(std::time(nullptr));
	Game * gra = Game::getInstancja();
	gra->rozgrywka();
	std::system("pause");
	return 0;
}