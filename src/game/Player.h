#pragma once
#include <exception>
#include <string>
#include <iostream>

#ifndef __player_h__
#define __player_h__

class Auto;

//(inaczej gracz), osoba kt�a kieruje samochodem

class Player
{
public:
	Player();
	~Player();
	private: std::string _name;
	private: int _wiek;
	public: Auto * _posiada;
	public: std::string ustawImie();
	public: int ustawWiek();
};

#endif
