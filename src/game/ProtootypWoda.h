#pragma once
#ifndef __PrototypWoda_h__
#define __PrototypWoda_h__

#include "Pulapka.h"
#include "PrototypPrzeszkody.h"
#include "Auto.h"

class PrototypWoda : public Pulapka
{
public:
	PrototypWoda();
	~PrototypWoda();

public: void wykryjKolizje();
		void woda();
		PrototypPrzeszkody* klonuj();

};

#endif

