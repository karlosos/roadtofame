#include "Auto.h"
#include "Pulapka.h"
#include "Player.h"
#include "Tor.h"

#include <iostream>
using namespace std;

Auto::Auto()
{
	//cout << "Konstruktor Auto" << endl
}

Auto::~Auto()
{
	//cout << "Destruktor Auto" << endl
}

bool Auto::zmienKolor() {
	cout << "Wybierz kolor : " << endl;
	cout << "1 - zielony  |  2 - czerwony  |  3 - zolty" << endl;
	cin >> this->_kolor;
	switch (_kolor) {
		case 1: cout << "Auto jest zielone" << endl; break;
		case 2: cout << "Auto jest czerwone" << endl; break;
		case 3: cout << "Auto jest zolte" << endl; break;
		default: cout << "Auto jest domyslnie biale" << endl; break;
	}
	return true;
}

bool Auto::wykryjuderzenie() {
	//jakosc trzeba sprawdzac x i y z Pulapka x i y
	cout << "Dosz�o do kolozji z przeszkoda" << endl;
	return true;
}

void Auto::zatankuj() {
	if (this->_paliwo == 0)
		cout << "Tankowanie auta do pelna" << endl;
	this->_paliwo = 1;
}

void Auto::setX(double x)
{
	this->_x = x;
}

void Auto::setY(double y)
{
	this->_y = y;
}

double Auto::getX()
{
	return _x;
}

double Auto::getY()
{
	return _y;
}

void Auto::odpalAuto() {
	if(this->_silnik == false){
		cout << "Auto ma pe�ny bak, wiec odpalamy silnik" << endl;
		this->_silnik = true;
	}
}

void Auto::jedzDoPrzodu()
{
	// jezeli hamuje to nie jedz to przodu, ale spusc hamulec
	// w nastepnym dodaniem gazu pojedzie
	if (_hamowanie == true)
		this->_hamowanie = false;
	else
		this->_x++;
}

void Auto::setHamuj() {
	this->_hamowanie = true;
}

bool Auto::getHamuj() {
	return this->_hamowanie;
}

void Auto::skrecWPrawo()
{
	this->_y++;
}

void Auto::skrecWLewo()
{
	this->_y--;
}
