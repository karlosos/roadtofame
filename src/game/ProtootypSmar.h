#pragma once
#ifndef __PrototypSmar_h__
#define __PrototypSmar_h__

#include "Pulapka.h"
#include "PrototypPrzeszkody.h"
#include "Auto.h"

class PrototypSmar : public Pulapka
{
public:
	PrototypSmar();
	~PrototypSmar();

public: void wykryjKolizje();
		void smar();
		PrototypPrzeszkody* klonuj();

};

#endif