#include "ProtootypWoda.h"
#include "Pulapka.h"
#include "PrototypPrzeszkody.h"
#include "Auto.h"
#include <iostream>
using namespace std;


PrototypWoda::PrototypWoda()
{
	//cout << "Konstruktor ProtootypWoda" << endl;
}


PrototypWoda::~PrototypWoda()
{
	//cout << "Destruktor ProtootypWoda" << endl;
}

void PrototypWoda::wykryjKolizje() {
	// sprawdz czy jest kolizja z autem
	if (this->_unnamed_Auto->getX() == this->_x && this->_unnamed_Auto->getY() == this->_y) {
		cout << "Auto wpadlo w kolizje z Woda" << endl;
	}
	else {
		// debugowanie kolizji
		//cout << " " << this->_unnamed_Auto->getX() << " " << this->_x << " " << this->_unnamed_Auto->getY() << " " << this->_y << std::endl;
	}
}

void PrototypWoda::woda() {
	PrototypWoda woda = PrototypWoda();
}

PrototypPrzeszkody* PrototypWoda::klonuj() {
	PrototypWoda* sklonowany;
	sklonowany = new PrototypWoda();
	sklonowany->setX(this->_x);
	sklonowany->setY(this->_y);
	sklonowany->_unnamed_Auto = this->_unnamed_Auto;
	return sklonowany;
}
