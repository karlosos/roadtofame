#pragma once
#ifndef __Game_h__
#define __Game_h__

#include "Player.h"
#include "Auto.h"
#include "Tor.h"
#include "ProtootypOpona.h"
#include "ProtootypSmar.h"
#include "ProtootypUbytekJezdni.h"
#include "ProtootypWoda.h"

class Game
{
private: Game();
public: ~Game();
public: static Game* getInstancja();
private: static Game * _instancja;
private: Player * _unnamed_player;
private: Tor * _unnamed_tor;
private: Auto * _unnamed_Auto;
private: vector<PrototypPrzeszkody*> przeszkody;
public: void rozgrywka();
private: void przywitanie();
private: void podajImieGracza();
private: void podajWiekGracza();
private: void przygotujAuto();
private: void przygotujTor();
private: void przygotujPulapki();
private: void generujPulapki(PrototypPrzeszkody* prot);
};

#endif

